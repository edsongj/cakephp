<?php

use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    // Register scoped middleware for in scopes.
    $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        'httpOnly' => true
    ]));

    $routes->applyMiddleware('csrf');
    
    // // Este trecho é destinado ao redirecionamento para o index de welcome 
    // //de forma direta, sem a necessidade de inserir o prefixo
    // $routes->connect('/', ['prefix' => 'admin', 'controller' => 'Welcome', 'action' => 'index']);
    
    // $routes->connect('/pages/*', ['prefix' => 'admin', 'controller' => 'Welcome', 'action' => 'index']);
    // // Fim do trecho da rota direta
    
    // Incluindo rota para home
    $routes->connect('/', ['controller' => 'Home', 'action' => 'index']);
    $routes->connect('/pages/*', ['controller' => 'Home', 'action' => 'index']);
    
    $routes->fallbacks(DashedRoute::class);
});

Router::prefix('admin', function (RouteBuilder $routes) {
    // Register scoped middleware for in scopes.
    $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        'httpOnly' => true
    ]));
    
    $routes->applyMiddleware('csrf');

    $routes->fallbacks(DashedRoute::class);
});