<main role="main">
    <div class="jumbotron contato">
    <div class="container">
        <h2 class="display-4 text-center contato-titulo">Contato</h2>

        <form>
        <div class="form-row">
            <div class="form-group col-md-6">
            <label>Nome</label>
            <input name="nome" type="text" class="form-control" id="nome" placeholder="Nome completo">
            </div>
            <div class="form-group col-md-6">
            <label>Email</label>
            <input name="email" type="email" class="form-control" id="email" placeholder="Seu melhor e-mail">
            </div>
        </div>
        <div class="form-group">
            <label>Assunto</label>
            <input name="assunto" type="text" class="form-control" id="assunto" placeholder="Assunto da mensagem">
        </div>

        <div class="form-group">
            <label for="inputAddress2">Mensagem</label>
            <textarea name="mensagem" class="form-control" id="mensagem" rows="4"></textarea>
        </div>

        
        <button type="submit" class="btn btn-info">Enviar</button>
        </form>

        <hr class="featurette-divider">
    </div>
    </div>
</main>