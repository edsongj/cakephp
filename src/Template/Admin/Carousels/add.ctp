<div class="d-flex">
    <div class="mr-auto p-2">
        <h2 class="display-4 titulo">Cadastrar Carousel</h2>
    </div>
        <div class="p-2">
            <span class="d-none d-md-block">
                <?= $this->Html->link(__('Listar'), ['controller' => 'Carousels', 'action' => 'index'], ['class' => 'btn btn-outline-info btn-sm']) ?>
            </span>
            <div class="dropdown d-block d-md-none">
                <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="acoesListar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Ações
                </button>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="acoesListar">
                    <?= $this->Html->link(__('Listar'), ['controller' => 'Carousels', 'action' => 'index'], ['class' => 'dropdown-item']) ?>                                  
                </div>
            </div>
        </div>
</div><hr>
<?= $this->Flash->render();?>
<?= $this->Form->create($carousel, ['enctype' => 'multipart/form-data']);?>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for='name'><span class="text-danger">*</span> Nome Carousel</label>
            <?= $this->Form->control('nome_carousel',['label'=> false, 'class'=>'form-control', 'placeholder' => 'Informe um nome para o Carousel', 'id' => 'name']);?>
        </div>
        <div class="form-group col-md-6">
            <label for='titulo'> Título</label>
            <?= $this->Form->control('titulo',['label'=> false, 'class'=>'form-control', 'placeholder' => 'Informe um título para o Carousel', 'id' => 'titulo']);?>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-12">
            <label for='descricao'> Descrição</label>
            <?= $this->Form->control('descricao',['label'=> false, 'class'=>'form-control', 'placeholder' => 'Informe uma descrição para o Carousel', 'id' => 'descricao']);?>
        </div>
        
    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for='titulo_botao'> Título Do Botão</label>
            <?= $this->Form->control('titulo_botao',['label'=> false, 'class'=>'form-control', 'placeholder' => 'Informe um título para o botão do Carousel', 'id' => 'titulo_botao']);?>
        </div>
        <div class="form-group col-md-4">
            <label for='link'> Link do Botão</label>
            <?= $this->Form->control('link',['label'=> false, 'class'=>'form-control', 'placeholder' => 'Informe uma descrição para o Carousel', 'id' => 'link']);?>
        </div>
        <div class="form-group col-md-4">
            <label for='color_id'> Cor do Botão</label>
            <?= $this->Form->control('color_id', ['label' => false, 'class' => 'form-control', 'options' => $colors, 'id' => 'color_id']);?>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for='position_id'><span class="text-danger">*</span> Posição do Texto</label>
            <?= $this->Form->control('position_id', ['label' => false, 'class' => 'form-control', 'options' => $positions, 'id' => 'position_id']);?>
        </div>
        
        <div class="form-group col-md-6">
            <label for='situation_id'><span class="text-danger">*</span> Situação Carousel</label>
            <?= $this->Form->control('situation_id',['label'=> false, 'class'=>'form-control', 'options' => $situations, 'id' => 'situation_id']);?>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <!-- Para trabalhar a mensagem de erro ao validar o campo deve ser alterado em src/Model/Table/UsersTable.ctp -->
            <label for='username'><span class="text-danger">*</span> Foto (1920 X 846)</label><br>
            
            <!-- Trecho para trabalhar com a mudança da imagem com o onchange em webroot/js/dashboard.js  -->
            <!-- <//?= $this->Form->file('imagem',['label'=> false, 'onchange' => 'previewImagem()']);?> -->
            <?= $this->Form->control('imagem',['type' => 'file', 'label'=> false, 'onchange' => 'previewImagem()']);?>
        </div>
        <div class="form-group col-md-6">
            <?php
                // debug($carousel);
                // debug($carousel->imagem);
                if($carousel->imagem !== null){
                    $imagem_antiga = '../../files/carousel/'.$carousel->id.'/'.$carousel->imagem;
                }else{
                    $imagem_antiga = '../../files/carousel/img-padrao.png';
                }
            ?>
            <img src='<?= $imagem_antiga ?>' alt='<?= $carousel->name?>' id="preview-img" class='img-thumbnail' style="width: 400px; height: 250px;">
        </div>
    </div>
    <p>
        <span class="text-danger">* </span>Campo obrigatório
    </p>
    <?= $this->Form->button(__('Cadastrar'), ['class'=>'btn btn-success'])?>
<?= $this->Form->end();?>
