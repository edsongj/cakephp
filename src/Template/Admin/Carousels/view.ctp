<div class="d-flex">
    <div class="mr-auto p-2">
        <h2 class="display-4 titulo">Carousel</h2>
    </div>
    <div class="p-2">
        <span class="d-none d-md-block">
            <?= $this->Html->link(__('Listar'), ['controller' => 'Carousels', 'action' => 'index', $carousel->id], ['class' => 'btn btn-outline-info btn-sm']) ?>
            <?= $this->Html->link(__('Editar'), ['action' => 'edit', $carousel->id], ['class' => 'btn btn-outline-warning btn-sm']) ?>
            <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $carousel->id], ['confirm' => __('Você tem certeza que deseja escluir o Carousel {0}?', $carousel->id), 'class' => 'btn btn-outline-danger btn-sm']) ?>
            
        </span>
        <div class="dropdown d-block d-md-none">
            <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="acoesListar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Ações
            </button>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="acoesListar">                                    
            <?= $this->Html->link(__('Listar'), ['controller' => 'Carousels', 'action' => 'index', $carousel->id], ['class' =>'dropdown-item']) ?>
            <?= $this->Html->link(__('Editar'), ['action' => 'edit', $carousel->id], ['class' => 'dropdown-item']) ?>
            <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $carousel->id], ['confirm' => __('Você tem certeza que deseja escluir o Carousel {0}?', $carousel->id), 'class' => 'dropdown-item']) ?>
            </div>
        </div>
    </div>
</div><hr>
<?= $this->Flash->render();?>
<dl class="row">
<dt class="col-sm-3">Imagem</dt>
    <dd class="col-sm-9">
        <div class="img-perfil">
            <?php if(!empty($carousel->imagem)){?>
                <!-- Inserindo imagem do usuário do cabeçalho -->
                <?= $this->Html->image('../files/carousel/'.$carousel->id.'/'.$carousel->imagem, ['class'=>'img-thumbnail', 'width'=>'400', 'height'=>'350'])?>&nbsp;
                <div class="edit">
                    <?= $this->Html->link(
                        '<i class="fas fa-pencil-alt"></i>', 
                        [
                            'controller' => 'Carousels',
                            'action' => 'alterarFotoCarousel',
                            $carousel->id
                        ],
                        [
                            'escape' => false
                        ]
                    )?>
                </div>
            <?php }else{?>
                <?= $this->Html->image('../files/carousel/img-padrao.png', ['class'=>'img-thumbnail', 'width'=>'400', 'height'=>'350'])?>&nbsp;
                <div class="edit">
                    <?= $this->Html->link(
                        '<i class="fas fa-pencil-alt"></i>', 
                        [
                            'controller' => 'Carousels',
                            'action' => 'alterarFotoCarousel',
                            $carousel->id
                        ],
                        [
                            'escape' => false
                        ]
                    )?>
                </div>
            <?php }?>
        </div>
    </dd>
    <dt class="col-sm-3">ID</dt>
    <dd class="col-sm-9"><?= $this->Number->format($carousel->id) ?></dd>

    <dt class="col-sm-3">Nome</dt>
    <dd class="col-sm-9"><?= h($carousel->nome_carousel) ?></dd>

    <dt class="col-sm-3">Título</dt>
    <dd class="col-sm-9"><?= h($carousel->titulo) ?></dd>
    
    <dt class="col-sm-3">Descrição</dt>
    <dd class="col-sm-9"><?= h($carousel->descricao) ?></dd>

    <dt class="col-sm-3">Titulo Botão</dt>
    <dd class="col-sm-9"><?= h($carousel->titulo_botao) ?></dd>
 

    <dt class="col-sm-3">Link do Botão</dt>
    <dd class="col-sm-9"><?= h($carousel->link) ?></dd>
 

    <dt class="col-sm-3">Cor do Botão</dt>
    <dd class="col-sm-9"><?= $this->Form->button(__($carousel->color->nome_cor), ['class' => 'btn btn-'.$carousel->color->cor])?>
    </dd>
 

    <dt class="col-sm-3">Posição do Texto</dt>
    <dd class="col-sm-9"><?= h($carousel->position->nome_posicao) ?></dd>
 

    <dt class="col-sm-3">Ordem do Slide</dt>
    <dd class="col-sm-9"><?= h($carousel->ordem) ?></dd>
 

    <dt class="col-sm-3">Situação</dt>
    <?php
        $cor = $carousel->situation->id;
        if($cor == 1){
            $cor = "badge badge-success";
        }elseif($cor == 2){
            $cor = "badge badge-danger";
        }else{
            $cor = "badge badge-primary";
        }
    ?>
    <dd class="col-sm-9">
        <?php
            echo "<span class='$cor'>".$carousel->situation->nome_situacao."</span>";
        ?>
    <dd>

    <dt class="col-sm-3 text-truncate">Data de Criação</dt>
    <dd class="col-sm-9"><?= h($carousel->created) ?></dd>

    <dt class="col-sm-3 text-truncate">Data de Modificação</dt>
    <dd class="col-sm-9"><?= h($carousel->modified) ?></dd>
 
</dl>