<div class="d-flex">
    <div class="mr-auto p-2">
        <h2 class="display-4 titulo">Listar Sobre Empresa</h2>
    </div>
        <div class="p-2">
            <?= $this->Html->link(__('Cadastrar'), ['controller' => 'EmpresasSobre', 'action' => 'add'], ['class' => 'btn btn-outline-success btn-sm']) ;?>
        </div>
    </a>
</div>
<!-- Mensengem de erro ou sucesso, está sendo trabalhado em src/Templace/Element/Flash -->
<?= $this->Flash->render();?>
<div class="table-responsive">
    <table class="table table-striped table-hover table-bordered">
        <thead>
            <tr>
                <th>ID</th>
                <th>Título</th>
                <th class="d-none d-sm-table-cell">Ordem</th>
                <th class="d-none d-lg-table-cell">Situação</th>
                <th class="text-center">Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($empresasSobre as $empresasSobre): ?>
                <tr>
                    <td><?= $this->Number->format($empresasSobre->id) ?></td>
                    <td><?= h($empresasSobre->titulo) ?></td>
                    <td class="d-none d-sm-table-cell"><?= h($empresasSobre->ordem) ?></td>
                    <?php
                        if($empresasSobre->situation->id == 1){
                            $cor = "badge badge-success";
                        }elseif($empresasSobre->situation->id == 2){
                            $cor = "badge badge-danger";
                        }else{
                            $cor = "badge badge-primary";
                        }
                    ?>
                    <td class="d-none d-lg-table-cell"><?= "<span class='$cor'>".$empresasSobre->situation->nome_situacao."</span>";?></td>
                    <td>
                        <span class="d-none d-md-block">
                            <?= $this->Html->link(__('<i class="fas fa-angle-double-up"></i>'), 
                                [
                                    'controller' => 'EmpresasSobre', 'action' => 'altOrdemEmpSob', $empresasSobre->id
                                ], 
                                [
                                    'class' => 'btn btn-outline-info btn-sm', 'escape' => false
                                ]) ?>
                            <?= $this->Html->link(__('Visualizar'), ['controller' => 'EmpresasSobre', 'action' => 'view', $empresasSobre->id], ['class' => 'btn btn-outline-primary btn-sm']) ?>
                            <?= $this->Html->link(__('Editar'), ['controller' => 'EmpresasSobre', 'action' => 'edit', $empresasSobre->id], ['class' => 'btn btn-outline-warning btn-sm']) ?>
                            <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $empresasSobre->id], ['class' => 'btn btn-outline-danger btn-sm', 'confirm' => __('Você tem certeza que deseja excluir Sobre Empresa {0}?', $empresasSobre->id)]) ?>
                        </span>
                        <div class="dropdown d-block d-md-none">
                            <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="acoesListar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Ações
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="acoesListar">
                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'EmpresasSobre', 'action' => 'view', $empresasSobre->id], ['class' => 'dropdown-item']) ?>
                                <?= $this->Html->link(__('Editar'), ['action' => 'edit', $empresasSobre->id], ['class' => 'dropdown-item']) ?>
                                <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $empresasSobre->id], ['class' => 'dropdown-item', 'confirm' => __('Você tem certeza que deseja excluir Sobre Empresa {0}?', $empresasSobre->id)]) ?>
                            </div>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>        
    </table>
    <?= $this->element('pagination');?>
</div>