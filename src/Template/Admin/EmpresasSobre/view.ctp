<div class="d-flex">
    <div class="mr-auto p-2">
        <h2 class="display-4 titulo">Sobre Empresas</h2>
    </div>
    <div class="p-2">
        <span class="d-none d-md-block">
            <?= $this->Html->link(__('Listar'), ['controller' => 'EmpresasSobre', 'action' => 'index', $empresasSobre->id], ['class' => 'btn btn-outline-info btn-sm']) ?>
            <?= $this->Html->link(__('Editar'), ['action' => 'edit', $empresasSobre->id], ['class' => 'btn btn-outline-warning btn-sm']) ?>
            <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $empresasSobre->id], ['confirm' => __('Você tem certeza que deseja excluir Sobre Empresas {0}?', $empresasSobre->id), 'class' => 'btn btn-outline-danger btn-sm']) ?>
            
        </span>
        <div class="dropdown d-block d-md-none">
            <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="acoesListar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Ações
            </button>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="acoesListar">                                    
            <?= $this->Html->link(__('Listar'), ['controller' => 'EmpresasSobre', 'action' => 'index', $empresasSobre->id], ['class' =>'dropdown-item']) ?>
            <?= $this->Html->link(__('Editar'), ['action' => 'edit', $empresasSobre->id], ['class' => 'dropdown-item']) ?>
            <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $empresasSobre->id], ['confirm' => __('Você tem certeza que deseja excluir Sobre Empresas {0}?', $empresasSobre->id), 'class' => 'dropdown-item']) ?>
            </div>
        </div>
    </div>
</div><hr>
<?= $this->Flash->render();?>
<dl class="row">
<dt class="col-sm-3">Imagem</dt>
    <dd class="col-sm-9">
        <div class="img-perfil">
            <?php if(!empty($empresasSobre->imagem)){?>
                <?= $this->Html->image('../files/sobre_empresas/'.$empresasSobre->id.'/'.$empresasSobre->imagem, ['class'=>'img-thumbnail', 'width'=>'250', 'height'=>'250'])?>&nbsp;
                <div class="edit">
                    <?= $this->Html->link(
                        '<i class="fas fa-pencil-alt"></i>', 
                        [
                            'controller' => 'EmpresasSobre',
                            'action' => 'alterarFotoSobEmp',
                            $empresasSobre->id
                        ],
                        [
                            'escape' => false
                        ]
                    )?>
                </div>
            <?php }else{?>
                <?= $this->Html->image('../files/sobre_empresas/img-padrao.png', ['class'=>'img-thumbnail', 'width'=>'350', 'height'=>'250'])?>&nbsp;
                <div class="edit">
                    <?= $this->Html->link(
                        '<i class="fas fa-pencil-alt"></i>', 
                        [
                            'controller' => 'EmpresasSobre',
                            'action' => 'alterarFotoSobEmp',
                            $empresasSobre->id
                        ],
                        [
                            'escape' => false
                        ]
                    )?>
                </div>
            <?php }?>
        </div>
    </dd>
    <dt class="col-sm-3">ID</dt>
    <dd class="col-sm-9"><?= $this->Number->format($empresasSobre->id) ?></dd>

    <dt class="col-sm-3">Título</dt>
    <dd class="col-sm-9"><?= h($empresasSobre->titulo) ?></dd>
    
    <dt class="col-sm-3">Descrição</dt>
    <dd class="col-sm-9"><?= h($empresasSobre->descricao) ?></dd>

    <dt class="col-sm-3">Ordem do Sobre Empresas</dt>
    <dd class="col-sm-9"><?= h($empresasSobre->ordem) ?></dd>
 

    <dt class="col-sm-3">Situação</dt>
    <?php
        $cor = $empresasSobre->situation->id;
        if($cor == 1){
            $cor = "badge badge-success";
        }elseif($cor == 2){
            $cor = "badge badge-danger";
        }else{
            $cor = "badge badge-primary";
        }
    ?>
    <dd class="col-sm-9">
        <?php
            echo "<span class='$cor'>".$empresasSobre->situation->nome_situacao."</span>";
        ?>
    <dd>

    <dt class="col-sm-3 text-truncate">Data de Criação</dt>
    <dd class="col-sm-9"><?= h($empresasSobre->created) ?></dd>

    <dt class="col-sm-3 text-truncate">Data de Modificação</dt>
    <dd class="col-sm-9"><?= h($empresasSobre->modified) ?></dd>
 
</dl>
