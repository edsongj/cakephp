<div class="d-flex">
    <div class="mr-auto p-2">
        <h2 class="display-4 titulo">Editar Depoimentos</h2>
    </div>
    <div class="p-2">
        <span class="d-none d-md-block">
            <?= $this->Html->link(__('Visualizar'), ['controller' => 'Depoimentos', 'action' => 'view', $depoimento->id], ['class' => 'btn btn-outline-primary btn-sm'])?>
        </span>
        <div class="dropdown d-block d-md-none">
            <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="acoesListar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Ações
            </button>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="acoesListar">
                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Depoimentos', 'action' => 'view', $depoimento->id], ['class' => 'dropdown-item']) ?>
            </div>
        </div>
    </div>
</div><hr>
<?= $this->Flash->render();?>
<?= $this->Form->create($depoimento);?>
    <div class="form-row">
        <div class="form-group col-md-7">
            <label for='nome_dep'><span class="text-danger">*</span> Nome do Depoimento</label>
            <?= $this->Form->control('nome_dep',['label'=> false, 'class'=>'form-control', 'placeholder' => 'Informe o nome para o Depoimento', 'id' => 'nome_dep']);?>
        </div>
        <div class="form-group col-md-10">
            <label for='descricao_dep'><span class="text-danger">*</span> Descrição do Depoimento</label>
            <?= $this->Form->control('descricao_dep',['label'=> false, 'class'=>'form-control', 'placeholder' => 'Informe o nome para o Depoimento', 'id' => 'descricao_dep']);?>
        </div>
        <div class="form-group col-md-10">
            <label for='video_um'><span class="text-danger">*</span> Vídeo Um</label>
            <?= $this->Form->control('video_um',['label'=> false, 'class'=>'form-control', 'placeholder' => 'Informe o nome para o Depoimento', 'id' => 'video_um']);?>
        </div>
        <div class="form-group col-md-10">
            <label for='video_dois'><span class="text-danger">*</span> Vídeo Dois</label>
            <?= $this->Form->control('video_dois',['label'=> false, 'class'=>'form-control', 'placeholder' => 'Informe o nome para o Depoimento', 'id' => 'video_dois']);?>
        </div>
        <div class="form-group col-md-10">
            <label for='video_tres'><span class="text-danger">*</span> Vídeo Três</label>
            <?= $this->Form->control('video_tres',['label'=> false, 'class'=>'form-control', 'placeholder' => 'Informe o nome para o Depoimento', 'id' => 'video_tres']);?>
        </div>
    </div>
    <p>
        <span class="text-danger">* </span>Campo obrigatório
    </p>
    <?= $this->Form->button(__('Salvar'), ['class'=>'btn btn-warning'])?>
<?= $this->Form->end() ?>
