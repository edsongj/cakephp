<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Depoimento $depoimento
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Depoimentos'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="depoimentos form large-9 medium-8 columns content">
    <?= $this->Form->create($depoimento) ?>
    <fieldset>
        <legend><?= __('Add Depoimento') ?></legend>
        <?php
            echo $this->Form->control('nome_dep');
            echo $this->Form->control('descricao_dep');
            echo $this->Form->control('vide_um');
            echo $this->Form->control('video_dois');
            echo $this->Form->control('video_tres');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
