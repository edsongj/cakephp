<div class="d-flex">
    <div class="mr-auto p-2">
        <h2 class="display-4 titulo">Depoimentos</h2>
    </div>
    <div class="p-2">
        <span class="d-none d-md-block">
            <?= $this->Html->link(__('Editar'), ['controller' => 'Depoimentos', 'action' => 'edit', $depoimento->id], ['class' => 'btn btn-outline-warning btn-sm'])?>
        </span>
        <div class="dropdown d-block d-md-none">
            <button class="btn btn-warning dropdown-toggle btn-sm" type="button" id="acoesListar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Ações
            </button>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="acoesListar">
                <?= $this->Html->link(__('Editar'), ['controller' => 'Depoimentos', 'action' => 'edit', $depoimento->id], ['class' => 'dropdown-item']) ?>
            </div>
        </div>
    </div>
</div><hr>
<?= $this->Flash->render();?>
<dl class="row">
    <dt class="col-sm-3">ID</dt>
    <dd class="col-sm-8"><?= h($depoimento->id) ?></dd>
</dl>
<dl class="row">
    <dt class="col-sm-3">Nome do Depoimento</dt>
    <dd class="col-sm-8"><?= h($depoimento->nome_dep) ?></dd>
</dl>
<dl class="row">
    <dt class="col-sm-3">Descrição</dt>
    <dd class="col-sm-8"><?= h($depoimento->descricao_dep) ?></dd>
</dl>
<dl class="row">
    <dt class="col-sm-3">Vídeo Um</dt>
    <dd class="col-sm-8"><?= $depoimento->video_um; ?></dd>
</dl>
<dl class="row">
    <dt class="col-sm-3">Vídeo Dois</dt>
    <dd class="col-sm-8"><?= $depoimento->video_dois; ?></dd>
</dl>
<dl class="row">
    <dt class="col-sm-3">Vídeo Três</dt>
    <dd class="col-sm-8"><?= $depoimento->video_tres ?></dd>
</dl>
<dl class="row">
    <dt class="col-sm-3">Data de Criação</dt>
    <dd class="col-sm-8"><?= h($depoimento->created) ?></dd>
</dl>
<dl class="row">
    <dt class="col-sm-3">Data de Modificação</dt>
    <dd class="col-sm-8"><?= h($depoimento->modified) ?></dd>
</dl>
