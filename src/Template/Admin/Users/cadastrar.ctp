<?= $this->Form->create($user, ['class' => 'form-signin']); ?>
<h1 class="h3 mb-3 font-weight-normal">Cadastrar</h1>

<!-- Redenrizando mensagem de erro que está em Template/Element/Flash/danger.ctp -->
    <!-- Observação eliminar mensagem de retorno quando usuário deslogado tentar acessar alguma página -->
<?= $this->Flash->render(); ?>

<div class="form-group">
    <div style="text-align: left;">
        <label>Nome</label>
    </div>
        <?= $this->Form->control('name', ['label' => false, 'class' => 'form-control', 'placeholder' => 'Informe seu nome completo']);?>
</div>
<div class="form-group">
    <div style="text-align: left;">
        <label>Email</label>
    </div>
        <?= $this->Form->control('email', ['label' => false, 'class' => 'form-control', 'placeholder' => 'Informe seu email']);?>
</div>
<div class="form-group">
    <div style="text-align: left;">
        <label>Usuário</label>
    </div>
        <?= $this->Form->control('username', ['label' => false, 'class' => 'form-control', 'placeholder' => 'Informe nome de usuário']);?>
</div>
<div class="form-group">
    <div style="text-align: left;">
        <label>Senha</label>
    </div>
    <?= $this->Form->control('password', ['label' => false, 'class' => 'form-control', 'placeholder' => 'Senha mínimo 6 caracteres']);?>
</div>
<?= $this->Form->button(__('Cadastrar'), ['class' => 'btn btn-lg btn-success btn-block']) ?>
<p>
    <?= $this->Html->link(__('Clique aqui'), ['controller' => 'Users', 'action' => 'login'])?>
    para acessar
</p>
<?= $this->Form->end() ?>