<div class="d-flex">
    <div class="mr-auto p-2">
        <h2 class="display-4 titulo">Usuário</h2>
    </div>
<div class="p-2">
    <span class="d-none d-md-block">
        <?= $this->Html->link(__('Listar'), ['controller' => 'users', 'action' => 'index', $user->id], ['class' => 'btn btn-outline-info btn-sm']) ?>
        <?= $this->Html->link(__('Editar'), ['action' => 'edit', $user->id], ['class' => 'btn btn-outline-warning btn-sm']) ?>
        <?= $this->Html->link(__('Editar Senha'), ['action' => 'editSenha', $user->id], ['class' => 'btn btn-outline-danger btn-sm']) ?>
        <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $user->id], ['confirm' => __('Você tem certeza que deseja excluir o usuário {0}?', $user->id), 'class' => 'btn btn-outline-danger btn-sm']) ?>
        
    </span>
    <div class="dropdown d-block d-md-none">
        <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="acoesListar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Ações
        </button>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="acoesListar">                                    
        <?= $this->Html->link(__('Listar'), ['controller' => 'users', 'action' => 'index', $user->id], ['class' =>'dropdown-item']) ?>
        <?= $this->Html->link(__('Editar'), ['action' => 'edit', $user->id], ['class' => 'dropdown-item']) ?>
        <?= $this->Html->link(__('Editar Senha'), ['action' => 'editSenha', $user->id], ['class' => 'dropdown-item']) ?>
        <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $user->id], ['confirm' => __('Você tem certeza que deseja excluir o usuário {0}?', $user->id), 'class' => 'dropdown-item']) ?>
        </div>
    </div>
</div>
</div><hr>
<?= $this->Flash->render();?>
<dl class="row">
<dt class="col-sm-3">Foto</dt>
    <dd class="col-sm-9">
        <div class="img-perfil">
            <?php if(!empty($user->imagem)){?>
                <!-- Inserindo imagem do usuário do cabeçalho -->
                <?= $this->Html->image('../files/user/'.$user->id.'/'.$user->imagem, ['class'=>'rounded-circle', 'width'=>'120', 'height'=>'120'])?>&nbsp;
                <div class="edit">
                    <?= $this->Html->link(
                        '<i class="fas fa-pencil-alt"></i>', 
                        [
                            'controller' => 'Users',
                            'action' => 'alterarFotoUsuario',
                            $user->id
                        ],
                        [
                            'escape' => false
                        ]
                    )?>
                </div>
            <?php }else{?>
                <?= $this->Html->image('../files/user/icone-user.png', ['class'=>'rounded-circle', 'width'=>'120', 'height'=>'120'])?>&nbsp;
                <div class="edit">
                    <?= $this->Html->link(
                        '<i class="fas fa-pencil-alt"></i>', 
                        [
                            'controller' => 'Users',
                            'action' => 'alterarFotoUsuario',
                            $user->id
                        ],
                        [
                            'escape' => false
                        ]
                    )?>
                </div>
            <?php }?>
        </div>
    </dd>
    <dt class="col-sm-3">ID</dt>
    <dd class="col-sm-9"><?= $this->Number->format($user->id) ?></dd>

    <dt class="col-sm-3">Nome</dt>
    <dd class="col-sm-9"><?= h($user->name) ?></dd>

    <dt class="col-sm-3">E-mail</dt>
    <dd class="col-sm-9"><?= h($user->email) ?></dd>
    
    <dt class="col-sm-3">Usuário</dt>
    <dd class="col-sm-9"><?= h($user->username) ?></dd>

    <dt class="col-sm-3 text-truncate">Data do Cadastro</dt>
    <dd class="col-sm-9"><?= h($user->created) ?></dd>

</dl>
