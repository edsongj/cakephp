<div class="d-flex">
    <div class="mr-auto p-2">
        <h2 class="display-4 titulo">Editar Foto</h2>
    </div>
    <div class="p-2">
        <span class="d-none d-md-block">
            <?= $this->Html->link(__('Visualizar'), ['controller' => 'users', 'action' => 'view', $user->id], ['class' => 'btn btn-outline-primary btn-sm'])?>
        </span>
        <div class="dropdown d-block d-md-none">
            <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="acoesListar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Ações
            </button>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="acoesListar">
                <?= $this->Html->link(__('Visualizar'), ['controller' => 'users', 'action' => 'view', $user->id], ['class' => 'dropdown-item']) ?>
            </div>
        </div>
    </div>
</div><hr>
<?= $this->Flash->render();?>
<?= $this->Form->create($user, ['enctype' => 'multipart/form-data']);?>
    <div class="form-row">
        <div class="form-group col-md-6">
            <!-- Para trabalhar a mensagem de erro ao validar o campo deve ser alterado em src/Model/Table/UsersTable.ctp -->
            <label for='username'><span class="text-danger">*</span> Foto (150 X 150)</label><br>
            
            <!-- Trecho para trabalhar com a mudança da imagem com o onchange em webroot/js/dashboard.js  -->
            <!-- <//?= $this->Form->file('imagem',['label'=> false, 'onchange' => 'previewImagem()']);?> -->
            <?= $this->Form->control('imagem',['type' => 'file', 'label'=> false, 'onchange' => 'previewImagem()']);?>
        </div>
        <div class="form-group col-md-6">
            <?php
                // debug($user);
                // debug($user->imagem);
                if($user->imagem !== ''){
                    $imagem_antiga = '../../../files/user/'.$user->id.'/'.$user->imagem;
                }else{
                    $imagem_antiga = '../../../files/user/incone_user.png';
                }
            ?>
            <img src='<?= $imagem_antiga ?>' alt='<?= $user->name?>' id="preview-img" class='img-thumbnail' style="width: 150px; height: 150px;">
        </div>
    </div>
    <p>
        <span class="text-danger">* </span>Campo obrigatório
    </p>
    <?= $this->Form->button(__('Salvar'), ['class'=>'btn btn-warning'])?>
<?= $this->Form->end();?>