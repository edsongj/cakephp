<div class="d-flex">
    <div class="mr-auto p-2">
        <h2 class="display-4 titulo">Editar Perfil</h2>
    </div>
    <div class="p-2">
        <span class="d-none d-md-block">
            <?= $this->Html->link(__('Visualizar'), ['controller' => 'users', 'action' => 'perfil', $user['id']], ['class' => 'btn btn-outline-primary btn-sm'])?>
        </span>
        <div class="dropdown d-block d-md-none">
            <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="acoesListar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Ações
            </button>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="acoesListar">
                <?= $this->Html->link(__('Visualizar'), ['controller' => 'users', 'action' => 'perfil', $user['id']], ['class' => 'dropdown-item']) ?>
            </div>
        </div>
    </div>
</div><hr>
<?= $this->Flash->render();?>
<?= $this->Form->create($user);?>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for='name'><span class="text-danger">*</span> Nome</label>
            <?= $this->Form->control('name',['label'=> false, 'class'=>'form-control', 'placeholder' => 'Nome completo', 'id' => 'name']);?>
        </div>
        <div class="form-group col-md-6">
            <!-- 
                * Para trabalhar a mensagem de erro ao validar o campo deve ser alterado em src/Model/Table/UsersTable.ctp 
                * Para personalizar a mensagem de erro deve atribuido a classe error-message no arquivo webroot/css/dashboard.css
            -->
            <label for='email'><span class="text-danger">*</span> E-mail</label>
            <?= $this->Form->control('email',['label'=> false, 'class'=>'form-control', 'placeholder' => 'Seu melhor e-mail', 'id' => 'email']);?>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-12">
            <!-- Para trabalhar a mensagem de erro ao validar o campo deve ser alterado em src/Model/Table/UsersTable.ctp -->
            <label for='username'><span class="text-danger">*</span> Usuário</label>
            <?= $this->Form->control('username',['label'=> false, 'class'=>'form-control', 'placeholder' => 'Informe Usuário', 'id' => 'username']);?>
        </div>
    </div>
    <p>
        <span class="text-danger">* </span>Campo obrigatório
    </p>
    <?= $this->Form->button(__('Salvar'), ['class'=>'btn btn-warning'])?>
<?= $this->Form->end()?>
