<div class="d-flex">
    <div class="mr-auto p-2">
        <h2 class="display-4 titulo">Serviços</h2>
    </div>
    <div class="p-2">
        <span class="d-none d-md-block">
            <?= $this->Html->link(__('Editar'), ['controller' => 'Servicos', 'action' => 'edit', $servico->id], ['class' => 'btn btn-outline-warning btn-sm'])?>
        </span>
        <div class="dropdown d-block d-md-none">
            <button class="btn btn-warning dropdown-toggle btn-sm" type="button" id="acoesListar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Ações
            </button>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="acoesListar">
                <?= $this->Html->link(__('Editar'), ['controller' => 'Servicos', 'action' => 'edit', $servico->id], ['class' => 'dropdown-item']) ?>
            </div>
        </div>
    </div>
</div><hr>
<?= $this->Flash->render();?>
<dl class="row">
    <dt class="col-sm-3">ID</dt>
    <dd class="col-sm-9"><?= h($servico->id) ?></dd>
    <dt class="col-sm-3">Título do Serviço</dt>
    <dd class="col-sm-9"><?= h($servico->titulo_ser) ?></dd>
    <dt class="col-sm-3">Data de Criação</dt>
    <dd class="col-sm-9"><?= h($servico->created) ?></dd>
    <dt class="col-sm-3">Data de Modificação</dt>
    <dd class="col-sm-9"><?= h($servico->modified) ?></dd>
</dl><hr>
<dl class="row">
    <dt class="col-sm-3">Titulo Um</dt>
    <dd class="col-sm-9"><?= h($servico->titulo_um) ?></dd>
    
    <dt class="col-sm-3">Icone Um</dt>
    <dd class="col-sm-9"><?= '<i class="fas fa-'.$servico->icone_um.'"></i> - '.h($servico->icone_um) ?></dd>
    
    <dt class="col-sm-3">Descrição Um</dt>
    <dd class="col-sm-9"><?= h($servico->descricao_um) ?></dd>
</dl><hr>
<dl class="row">
    <dt class="col-sm-3">Titulo Dois</dt>
    <dd class="col-sm-9"><?= h($servico->titulo_dois) ?></dd>
    
    <dt class="col-sm-3">Icone Dois</dt>
    <dd class="col-sm-9"><?= '<i class="fas fa-'.$servico->icone_dois.'"></i> - '.h($servico->icone_dois) ?></dd>
    
    <dt class="col-sm-3">Descrição Dois</dt>
    <dd class="col-sm-9"><?= h($servico->descricao_dois) ?></dd>
</dl><hr>
<dl class="row">
    <dt class="col-sm-3">Titulo Três</dt>
    <dd class="col-sm-9"><?= h($servico->titulo_tres) ?></dd>
    
    <dt class="col-sm-3">Icone Três</dt>
    <dd class="col-sm-9"><?= '<i class="fas fa-'.$servico->icone_tres.'"></i> - '.h($servico->icone_tres) ?></dd>
    
    <dt class="col-sm-3">Descrição Três</dt>
    <dd class="col-sm-9"><?= h($servico->descricao_tres) ?></dd>
</dl>