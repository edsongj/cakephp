<div class="d-flex">
    <div class="mr-auto p-2">
        <h2 class="display-4 titulo">Editar Serviços</h2>
    </div>
    <div class="p-2">
        <span class="d-none d-md-block">
            <?= $this->Html->link(__('Visualizar'), ['controller' => 'Servicos', 'action' => 'view', $servico->id], ['class' => 'btn btn-outline-primary btn-sm'])?>
        </span>
        <div class="dropdown d-block d-md-none">
            <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="acoesListar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Ações
            </button>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="acoesListar">
                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Servicos', 'action' => 'view', $servico->id], ['class' => 'dropdown-item']) ?>
            </div>
        </div>
    </div>
</div><hr>
<?= $this->Flash->render();?>
<?= $this->Form->create($servico);?>
    <div class="form-row">
        <div class="form-group col-md-12">
            <!-- Para trabalhar a mensagem de erro ao validar o campo deve ser alterado em src/Model/Table/UsersTable.ctp -->
            <label for='titulo_serv'><span class="text-danger">*</span> Título do Serviço</label>
            <?= $this->Form->control('titulo_ser',['label'=> false, 'class'=>'form-control', 'placeholder' => 'Informe Título Para O Serviço', 'id' => 'titulo_serv']);?>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for='titulo_um'><span class="text-danger">*</span> Título Um</label>
            <?= $this->Form->control('titulo_um',['label'=> false, 'class'=>'form-control', 'placeholder' => 'Informe Título Um', 'id' => 'titulo_um']);?>
        </div>
        <div class="form-group col-md-4">
            <label for='icone_um'><span class="text-danger">*</span> Icone Um</label>
            <?= $this->Form->control('icone_um',['label'=> false, 'class'=>'form-control', 'placeholder' => 'Informe Icone Um', 'id' => 'icone_um']);?>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-12">
            <label for='descricao_um'><span class="text-danger">*</span> Descrição Um</label>
            <?= $this->Form->control('descricao_um',['label'=> false, 'class'=>'form-control', 'placeholder' => 'Informe Icone Três', 'id' => 'descricao_um']);?>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for='titulo_dois'><span class="text-danger">*</span> Título Dois</label>
            <?= $this->Form->control('titulo_dois',['label'=> false, 'class'=>'form-control', 'placeholder' => 'Informe Título Dois', 'id' => 'titulo_dois']);?>
        </div>
        <div class="form-group col-md-4">
            <label for='icone_dois'><span class="text-danger">*</span> Icone Dois</label>
            <?= $this->Form->control('icone_dois',['label'=> false, 'class'=>'form-control', 'placeholder' => 'Informe Icone Dois', 'id' => 'icone_dois']);?>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-12">
            <label for='descricao_dois'><span class="text-danger">*</span> Descrição Dois</label>
            <?= $this->Form->control('descricao_dois',['label'=> false, 'class'=>'form-control', 'placeholder' => 'Informe Icone Três', 'id' => 'descricao_dois']);?>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for='titulo_tres'><span class="text-danger">*</span> Título Três</label>
            <?= $this->Form->control('titulo_tres',['label'=> false, 'class'=>'form-control', 'placeholder' => 'Informe Título Três', 'id' => 'titulo_tres']);?>
        </div>
        <div class="form-group col-md-4">
            <label for='icone_tres'><span class="text-danger">*</span> Icone Três</label>
            <?= $this->Form->control('icone_tres',['label'=> false, 'class'=>'form-control', 'placeholder' => 'Informe Icone Três', 'id' => 'icone_tres']);?>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-12">
            <label for='descricao_tres'><span class="text-danger">*</span> Descrição Três</label>
            <?= $this->Form->control('descricao_tres',['label'=> false, 'class'=>'form-control', 'placeholder' => 'Informe Icone Três', 'id' => 'descricao_tres']);?>
        </div>
    </div>
    <p>
        <span class="text-danger">* </span>Campo obrigatório
    </p>
    <?= $this->Form->button(__('Salvar'), ['class'=>'btn btn-warning'])?>
<?= $this->Form->end() ?>