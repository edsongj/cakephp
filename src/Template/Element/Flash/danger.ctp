<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>

<!-- Criando com bootstrap a mensagem de sucesso com botão para fechar a mensagem -->
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    
    <!-- A mensagem de erro está vindo do arquivo AppController no método do login -->
    <?= $message ?>
  
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>