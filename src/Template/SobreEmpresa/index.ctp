<main role="main">
    <div class="jumbotron sobre-empresa">
    <div class="container">
        <h2 class="display-4 text-center sob-emp-titulo">Sobre a Empresa</h2>

        <div class="row featurette">
        <div class="col-md-7 order-md-2">
            <h2 class="featurette-heading">Sobre Empresa Um</h2>
            <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>
        <div class="col-md-5 order-md-1">
            <img class="featurette-image img-fluid mx-auto" src="imagens/empresa1.jpg" alt="Sobre Empresa Um">
        </div>
        </div>

        <hr class="featurette-divider">

        <div class="row featurette">
        <div class="col-md-7">
            <h2 class="featurette-heading">Sobre Empresa Dois</h2>
            <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>
        <div class="col-md-5">
            <img class="featurette-image img-fluid mx-auto" src="imagens/empresa2.jpg" alt="Sobre Empresa Dois">
        </div>
        </div>

        <hr class="featurette-divider">
    </div>
    </div>
</main>