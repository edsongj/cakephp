<?php
    $cakeDescription = "Home";
?>
<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?= $cakeDescription ?>:
            <?= $this->fetch('title') ?>
        </title>
        <?= $this->Html->meta('icon') ?>

        <?= $this->Html->css(['bootstrap.min', 'fontawesome.min', 'personalsite']);?>
        <?= $this->Html->script(['fontawesome-all.min']);?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
    </head>
    <body>
        <?= $this->element('menu_site');?>
        
        <?= $this->Flash->render()?>
        <?= $this->fetch('content')?>

        <?= $this->element('rodape_site');?>

        <?= $this->Html->script(['jquery-3.4.1.min', 'popper.min', 'bootstrap.min']);?>
    </body>
</html>
