<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Mailer\MailerAwareTrait;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    // Atribuindo permissão para acessar a página cadastrar.ctp para cadastrar novo usuário
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['cadastrar', 'logout']);
    }

    public function index()
    {
        $this->paginate = [
            'limit' => 5
        ];
        $users = $this->paginate($this->Users);
        $this->set(compact('users'));
    }

    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
    }
    
    public function perfil()
    {
        $user_id = $this->Auth->user('id');
        $user = $this->Users->get($user_id);

        $this->set(compact('user'));
    }

    public function editPerfil()
    {
        $user_id = $this->Auth->user('id');
        $user = $this->Users->get($user_id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                
                $this->Flash->success(__('Perfil editado com sucesso'));

                return $this->redirect(['controller' => 'Users', 'action' => 'perfil']);
            }
            $this->Flash->error(__('Erro: Perfil não editado com sucesso'));
        }
        //  debug($user);
        $this->set(compact('user'));
    }

    public function editSenhaPerfil()
    {
        // Trecho pega os dados do usuário logado e envia para a view editSenhaPerfil
        $user_id = $this->Auth->user('id');
        $user = $this->Users->get($user_id,
            [
                'contain' => []
            ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Senha editada com sucesso'));

                return $this->redirect(['controller' => 'Users', 'action' => 'perfil']);
            }
            $this->Flash->danger(__('Erro: Senha não editada com sucesso'));
        }
        // debug($user);
        $this->set(compact('user'));
    }

    public function alterarFotoUsuario($id = null)
    {
        // // Capturando id do usuário logado
        // $user_id = $this->Auth->user($id);

        // Capturando todos os dados do usuário que pertence ao id já capturado
        $user = $this->Users->get($id);

        // Salvando o nome da imagem atual do usuário que já foi capturado
        $imagemAntiga = $user->imagem;

        
        // Verificando a informação enviada pelo usuário se foi um patch, post ou put
        if($this->request->is(['patch', 'post', 'put'])){
            // Instanciando model Users
            $user = $this->Users->newEntity();
        
            // Trabalhando caracteres especiais no nome da imagem com o Slug
            $user->imagem = $this->Users->slugUploadImgRed($this->request->getData()['imagem']['name']);
            
            $user->id = $id;
            // Salvando o nome da imagem no banco de dados
            if($this->Users->save($user)){
                // Recebendo os dados do formulário
                $user = $this->Users->patchEntity($user, $this->request->getData());
            
                $destino = WWW_ROOT. "files" . DS . "user" . DS . $id . DS;
                $imgUpload = $this->request->getData()['imagem'];
                $imgUpload['name'] = $user->imagem;
                if($user->imagem = $this->Users->uploadImgRed($imgUpload, $destino, 150, 150)){
                    $this->Users->deleteFile($destino, $imagemAntiga, $user->imagem);
                    $this->Flash->success(__('Foto editada com sucesso'));
                    
                    // Para retornar para a view do usuário que foi editado é necessário informar o id 
                    return $this->redirect(['controller' => 'Users', 'action' => 'view', $id]);
                }else{
                    $user->imagem = $imagemAntiga;
                    $this->Flash->danger(__('Erro: Foto não foi editada com sucesso'));
                }
            }
        }
        
        $this->set(compact('user'));
    }
    // public function alterarFotoUsuario($id = null)
    // {
    //     // // Capturando id do usuário logado
    //     // $user_id = $this->Auth->user($id);

    //     // Capturando todos os dados do usuário que pertence ao id já capturado
    //     $user = $this->Users->get($id);

    //     // Salvando o nome da imagem atual do usuário que já foi capturado
    //     $imagemAntiga = $user->imagem;

        
    //     // Verificando a informação enviada pelo usuário se foi um patch, post ou put
    //     if($this->request->is(['patch', 'post', 'put'])){
    //         // Instanciando model Users
    //         $user = $this->Users->newEntity();
        
    //         // Trabalhando caracteres especiais no nome da imagem com o Slug
    //         $user->imagem = $this->Users->slugSingleUpload($this->request->getData()['imagem']['name']);
            
    //         $user->id = $id;
    //         // Salvando o nome da imagem no banco de dados
    //         if($this->Users->save($user)){
    //             // Recebendo os dados do formulário
    //             $user = $this->Users->patchEntity($user, $this->request->getData());
            
    //             $destino = WWW_ROOT. "files" . DS . "user" . DS . $id . DS;
    //             $imgUpload = $this->request->getData()['imagem'];
    //             $imgUpload['name'] = $user->imagem;
    //             if($user->imagem = $this->Users->singleUpload($imgUpload, $destino)){
    //                 // Dependendo da estrutura do banco de dados deve ser trocado esse trecho
    //                 // if(($imagemAntiga !== null) AND ($imagemAntiga !== $user->imagem)){
    //                     // por esse abaixo
    //                 if(($imagemAntiga !== '') AND ($imagemAntiga !== $user->imagem)){
    //                     unlink($destino.$imagemAntiga);
    //                 }
    //                 $this->Flash->success(__('Foto editada com sucesso'));
    //                 return $this->redirect(['controller' => 'Users', 'action' => 'view', $id]);
    //             }else{
    //                 $user->imagem = $imagemAntiga;
    //                 $this->Flash->danger(__('Erro: Foto não foi editada com sucesso'));
    //             }
    //         }
    //     }
        
    //     $this->set(compact('user'));
    // }
    
    public function alterarFotoPerfil()
    {
        // Capturando id do usuário logado
        $user_id = $this->Auth->user('id');

        // Capturando todos os dados do usuário que pertence ao id já capturado
        $user = $this->Users->get($user_id);

        // Salvando o nome da imagem atual do usuário que já foi capturado
        $imagemAntiga = $user->imagem;

        
        // Verificando a informação enviada pelo usuário se foi um patch, post ou put
        if($this->request->is(['patch', 'post', 'put'])){
            // Instanciando model Users
            $user = $this->Users->newEntity();
        
            // Trabalhando caracteres especiais no nome da imagem com o Slug
            $user->imagem = $this->Users->slugUploadImgRed($this->request->getData()['imagem']['name']);
            
            $user->id = $user_id;
            // Salvando o nome da imagem no banco de dados
            if($this->Users->save($user)){
                // Recebendo os dados do formulário
                $user = $this->Users->patchEntity($user, $this->request->getData());
            
                $destino = WWW_ROOT. "files" . DS . "user" . DS . $user_id . DS;
                $imgUpload = $this->request->getData()['imagem'];
                $imgUpload['name'] = $user->imagem;
                if($user->imagem = $this->Users->uploadImgRed($imgUpload, $destino, 150, 150)){
                    $this->Users->deleteFile($destino, $imagemAntiga, $user->imagem);
                    $this->Flash->success(__('Foto editada com sucesso'));
                    return $this->redirect(['controller' => 'Users', 'action' => 'perfil']);
                }else{
                    $user->imagem = $imagemAntiga;
                    $this->Flash->danger(__('Erro: Foto não foi editada com sucesso'));
                }
            }
        }
        
        $this->set(compact('user'));
    }
    // public function alterarFotoPerfil()
    // {
    //     // Capturando id do usuário logado
    //     $user_id = $this->Auth->user('id');

    //     // Capturando todos os dados do usuário que pertence ao id já capturado
    //     $user = $this->Users->get($user_id);

    //     // Salvando o nome da imagem atual do usuário que já foi capturado
    //     $imagemAntiga = $user->imagem;

        
    //     // Verificando a informação enviada pelo usuário se foi um patch, post ou put
    //     if($this->request->is(['patch', 'post', 'put'])){
    //         // Instanciando model Users
    //         $user = $this->Users->newEntity();
        
    //         // Trabalhando caracteres especiais no nome da imagem com o Slug
    //         $user->imagem = $this->Users->slugSingleUpload($this->request->getData()['imagem']['name']);
            
    //         $user->id = $user_id;
    //         // Salvando o nome da imagem no banco de dados
    //         if($this->Users->save($user)){
    //             // Recebendo os dados do formulário
    //             $user = $this->Users->patchEntity($user, $this->request->getData());
            
    //             $destino = WWW_ROOT. "files" . DS . "user" . DS . $user_id . DS;
    //             $imgUpload = $this->request->getData()['imagem'];
    //             $imgUpload['name'] = $user->imagem;
    //             if($user->imagem = $this->Users->singleUpload($imgUpload, $destino)){
    //                 // Dependendo da estrutura do banco de dados deve ser trocado esse trecho
    //                 // if(($imagemAntiga !== null) AND ($imagemAntiga !== $user->imagem)){
    //                     // por esse abaixo
    //                 if(($imagemAntiga !== '') AND ($imagemAntiga !== $user->imagem)){
    //                     unlink($destino.$imagemAntiga);
    //                 }
    //                 $this->Flash->success(__('Foto editada com sucesso'));
    //                 return $this->redirect(['controller' => 'Users', 'action' => 'perfil']);
    //             }else{
    //                 $user->imagem = $imagemAntiga;
    //                 $this->Flash->danger(__('Erro: Foto não foi editada com sucesso'));
    //             }
    //         }
    //     }
        
    //     $this->set(compact('user'));
    // }
    
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                
                // Para utilizar o método Flash é necessário carregar em Controller/AppController.php
                    // As mensagens de sucesso e erro estão em src/Template/Element/Flash
                $this->Flash->success(__('Usuário cadastrado com Sucesso'));
                return $this->redirect(['action' => 'index']);

            }else{

                $this->Flash->danger(__('Erro: Usuário não cadastrado com sucesso'));
            }
        }
        $this->set(compact('user'));
    }

    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Usuário editado com sucesso'));

                // Para retornar para a view do usuário que foi editado é necessário informar o id
                return $this->redirect(['controller' => 'Users', 'action' => 'view', $id]);
            }
            $this->Flash->error(__('Erro: Usuário não editado com sucesso'));
        }
        $this->set(compact('user'));
    }
    
    public function editSenha($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Senha do usuário editado com sucesso'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Erro: Senha do usuário não editado com sucesso'));
        }
        $this->set(compact('user'));
    }
    
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        $destino = WWW_ROOT. "files" . DS . "user" . DS . $user->id . DS;
        $this->Users->deleteArq($destino);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('Usuário excluído com sucesso'));
        } else {
            $this->Flash->danger(__('Erro: Usuário não excluído com sucesso'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function login()
    {
        //Para utilizar o método Auth é necessário carregar em Controller/AppController.php
        if ($this->request->is(['post'])) {
            $user = $this->Auth->identify();
            //debug($user);

            //Método criado em aula, envia qualquer usuário logado para página do index.ctp de welcome
            if($user){
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            
            // Caso o usuário não consiga logar, esse else traz para a página de login a mensagem de erro
            }else{
                $this->Flash->danger(__('Usuário ou Senha estão incorretos'));
            }

            /**
            * Método criado por mim para redirecionar o usuário definido como adm para entrar 
            * na página index.ctp de users que contém as ações de manipulação dos usuários no banco
            */
            // if($user){
            //    if($user['username'] == 'edson'){
            //         $this->Auth->setUser($user);
            //         return $this->redirect(['controller' => 'users', 'action' => 'index']);
            //     }else{
            //         $this->Auth->setUser($user);
            //         return $this->redirect($this->Auth->redirectUrl());
            //    }
            // }
        }
    }
    public function logout()
    {
        $this->Flash->success(__('Usuário deslogado com sucesso'));
        return $this->redirect($this->Auth->logout());
    }

    use MailerAwareTrait;
    public function cadastrar()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                
                $this->getMailer('User')->send('cadastroUser', [$user]);
                // $email = new Email('enviaremail');
                // Para utilizar o método Flash é necessário carregar em Controller/AppController.php
                    // As mensagens de sucesso e erro estão em src/Template/Element/Flash
                $this->Flash->success(__('Novo usuário cadastrado com Sucesso'));
                return $this->redirect(['controller' => 'Users', 'action' => 'login']);

            }else{

                $this->Flash->danger(__('Erro: Novo usuário não foi cadastrado com sucesso'));
            }
        }
        $this->set(compact('user'));
    }
}
