<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Depoimentos Controller
 *
 * @property \App\Model\Table\DepoimentosTable $Depoimentos
 *
 * @method \App\Model\Entity\Depoimento[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DepoimentosController extends AppController
{

    public function index()
    {
        // Método getIdDepoimentos está em src/Model/Table/DepoimentosTable, craido por mim, para capturar o primeiro ID da tabela Depoimentos
        $depoimento = $this->Depoimentos->getIdDepoimentos();
        return $this->redirect(['controller' => 'Depoimentos', 'action' => 'view', $depoimento->id]);
    }

    public function view($id = null)
    {
        $depoimento = $this->Depoimentos->get($id, [
            'contain' => []
        ]);

        $this->set('depoimento', $depoimento);
    }
    
    // Método add não será utilizado
    // public function add()
    // {
    //     $depoimento = $this->Depoimentos->newEntity();
    //     if ($this->request->is('post')) {
    //         $depoimento = $this->Depoimentos->patchEntity($depoimento, $this->request->getData());
    //         if ($this->Depoimentos->save($depoimento)) {
    //             $this->Flash->success(__('The depoimento has been saved.'));

    //             return $this->redirect(['action' => 'index']);
    //         }
    //         $this->Flash->error(__('The depoimento could not be saved. Please, try again.'));
    //     }
    //     $this->set(compact('depoimento'));
    // }

    public function edit($id = null)
    {
        $depoimento = $this->Depoimentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $depoimento = $this->Depoimentos->patchEntity($depoimento, $this->request->getData());
            if ($this->Depoimentos->save($depoimento)) {
                $this->Flash->success(__('O Depoimento foi editado com sucesso.'));

                // Redireciona para a view passando o id desejado
                return $this->redirect(['controller' => 'Depoimentos', 'action' => 'view', $depoimento->id]);
                
                // Redireciona para a view passando o id desejado
                // return $this->redirect(['controller' => 'Depoimentos', 'action' => 'view', '1']);
            }
            $this->Flash->danger(__('Erro: O Depoimento não foi editado com sucesso.'));
        }
        $this->set(compact('depoimento'));
    }

    // Método delete não será utilizado 
    // public function delete($id = null)
    // {
    //     $this->request->allowMethod(['post', 'delete']);
    //     $depoimento = $this->Depoimentos->get($id);
    //     if ($this->Depoimentos->delete($depoimento)) {
    //         $this->Flash->success(__('The depoimento has been deleted.'));
    //     } else {
    //         $this->Flash->error(__('The depoimento could not be deleted. Please, try again.'));
    //     }

    //     return $this->redirect(['action' => 'index']);
    // }
}
