<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Servicos Controller
 *
 * @property \App\Model\Table\ServicosTable $Servicos
 *
 * @method \App\Model\Entity\Servico[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ServicosController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */

    
    // #Não será utilizado no projeto
    // public function index()
    // {
    //     $servicos = $this->paginate($this->Servicos);

    //     $this->set(compact('servicos'));
    // }

    /**
     * View method
     *
     * @param string|null $id Servico id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $servico = $this->Servicos->get($id, [
            'contain' => []
        ]);

        $this->set('servico', $servico);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */

    // #Não será utilizado no projeto
    // public function add()
    // {
    //     $servico = $this->Servicos->newEntity();
    //     if ($this->request->is('post')) {
    //         $servico = $this->Servicos->patchEntity($servico, $this->request->getData());
    //         if ($this->Servicos->save($servico)) {
    //             $this->Flash->success(__('The servico has been saved.'));

    //             return $this->redirect(['action' => 'index']);
    //         }
    //         $this->Flash->error(__('The servico could not be saved. Please, try again.'));
    //     }
    //     $this->set(compact('servico'));
    // }

    /**
     * Edit method
     *
     * @param string|null $id Servico id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $servico = $this->Servicos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $servico = $this->Servicos->patchEntity($servico, $this->request->getData());
            if ($this->Servicos->save($servico)) {
                $this->Flash->success(__('Serviço alterado com sucesso'));
                
                // Para retornar para a view do serviço que foi editado é necessário informar o id
                return $this->redirect(['controller' => 'Servicos', 'action' => 'view', $id]);
            }
            $this->Flash->danger(__('Erro: Serviço não foi alterado com sucesso'));
        }
        $this->set(compact('servico'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Servico id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $servico = $this->Servicos->get($id);
        if ($this->Servicos->delete($servico)) {
            $this->Flash->success(__('The servico has been deleted.'));
        } else {
            $this->Flash->error(__('The servico could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
