<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * EmpresasSobre Controller
 *
 * @property \App\Model\Table\EmpresasSobreTable $EmpresasSobre
 *
 * @method \App\Model\Entity\EmpresasSobre[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EmpresasSobreController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Situations'],
            'order' => ['EmpresasSobre.ordem' => 'ASC']
        ];
        $empresasSobre = $this->paginate($this->EmpresasSobre);

        $this->set(compact('empresasSobre'));
    }

    /**
     * View method
     *
     * @param string|null $id Empresas Sobre id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $empresasSobre = $this->EmpresasSobre->get($id, [
            'contain' => ['Situations']
        ]);

        $this->set('empresasSobre', $empresasSobre);
    }

    public function altOrdemEmpSob($id = null)
    {
        //  TableRegistry permite buscar informações na tabela EmpresasSobre
        $empresasSobreTable = TableRegistry::get('EmpresasSobre');
        
        // Método getSlideAtual está na Model/Table/EmpresasSobreTable e pega o Img atual 
        $imgAtual = $empresasSobreTable->getImgAtual($id);
        // debug($imgAtual);
        
        // Método getImgMenor está na Model/Table/EmpresasSobreTable e pega o Img de ordem superior ao atual
        $ordemMenor = $imgAtual->ordem - 1;
        $imgMenor = $empresasSobreTable->getImgMenor($ordemMenor);
        // debug($imgMenor); 
        
        // Verificando se existe algum Slide menor para realizar a aleração
        if($imgMenor){
            // Realizando updade no banco p/ alterar as ordens dos Slides
            $empresasSobreAtual = $this->EmpresasSobre->newEntity();
            $empresasSobreAtual->id = $imgAtual->id;
            $empresasSobreAtual->ordem = $imgAtual->ordem - 1;
            $this->EmpresasSobre->save($empresasSobreAtual);
            
            $empresasSobreMenor = $this->EmpresasSobre->newEntity();
            $empresasSobreMenor->id = $imgMenor->id;
            $empresasSobreMenor->ordem = $imgMenor->ordem + 1;
            $this->EmpresasSobre->save($empresasSobreMenor);
    
            $this->Flash->success(__('Ordem alterada com sucesso'));
            return $this->redirect(['controller' => 'EmpresasSobre', 'action' => 'index']);
        }else{
            $this->Flash->danger(__('Não existe outro slide de ordem menor para realizar a alteração'));
            return $this->redirect(['controller' => 'EmpresasSobre', 'action' => 'index']);
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $empresasSobre = $this->EmpresasSobre->newEntity();
        if ($this->request->is('post')) {

            $empresasSobre = $this->EmpresasSobre->patchEntity($empresasSobre, $this->request->getData());
            debug($empresasSobre);
            if(!$empresasSobre->errors()){
                $empresasSobre->imagem = $this->EmpresasSobre->slugUploadImgRed($this->request->getData()['imagem']['name']);
                $empresasSobreTable = TableRegistry::get('EmpresasSobre');
                $ultimaImgEmp = $empresasSobreTable->getUltimoImgEmp();
                $empresasSobre['ordem'] = $ultimaImgEmp['ordem'] + 1;
                if ($resultSave = $this->EmpresasSobre->save($empresasSobre)){
                    $id = $resultSave->id; // último id inserido
                    $destino = WWW_ROOT. "files" . DS . "sobre_empresas" . DS . $id . DS;                
                    $imgUpload = $this->request->getData()['imagem'];
                    $imgUpload['name'] = $empresasSobre->imagem;

                    if($this->EmpresasSobre->uploadImgRed($imgUpload, $destino, 500, 400)){
                        $this->Flash->success(__('Imagem cadastrada com sucesso'));
                        return $this->redirect(['controller' => 'EmpresasSobre', 'action' => 'view', $id]);
                    }else{
                        $this->Flash->danger(__('Erro: Imagem não foi cadastrada com sucesso. Erro ao realizar o upload'));
                    }
                }else{
                    $this->Flash->error(__('Erro: Imagem do Sobre Empresas não foi cadastrado com sucesso'));
                }    
            }else{
                $this->Flash->error(__('Erro: Imagem do Sobre Empresas não foi cadastrado com sucesso'));
            } 
    
        }
        $situations = $this->EmpresasSobre->Situations->find('list', ['limit' => 200]);
        $this->set(compact('empresasSobre', 'situations'));
    }
    /**
     * Edit method
     *
     * @param string|null $id Empresas Sobre id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $empresasSobre = $this->EmpresasSobre->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $empresasSobre = $this->EmpresasSobre->patchEntity($empresasSobre, $this->request->getData());
            if ($this->EmpresasSobre->save($empresasSobre)) {
                $this->Flash->success(__('A Sobre Empresas foi salvo com sucesso'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Erro: A Sobre Empresas não foi salvo com sucesso'));
        }
        $situations = $this->EmpresasSobre->Situations->find('list', ['limit' => 200]);
        $this->set(compact('empresasSobre', 'situations'));
    }

    public function alterarFotoSobEmp($id = null)
    {
        $empresasSobre = $this->EmpresasSobre->get($id);
        $imagemAntiga = $empresasSobre->imagem;

        if($this->request->is(['patch', 'post', 'put'])){
            $empresasSobre = $this->EmpresasSobre->newEntity();
            $empresasSobre = $this->EmpresasSobre->patchEntity($empresasSobre, $this->request->getData());
            if(!$empresasSobre->errors()){
                $empresasSobre->imagem = $this->EmpresasSobre->slugUploadImgRed($this->request->getData()['imagem']['name']);
                $empresasSobre->id = $id;
                if($this->EmpresasSobre->save($empresasSobre)){
                    $destino = WWW_ROOT. "files" . DS . "sobre_empresas" . DS . $id . DS;                
                    $imgUpload = $this->request->getData()['imagem'];
                    $imgUpload['name'] = $empresasSobre->imagem;
                    
                    if($this->EmpresasSobre->uploadImgRed($imgUpload, $destino, 500, 400)){
                        $this->EmpresasSobre->deleteFile($destino, $imagemAntiga, $empresasSobre->imagem);
                        $this->Flash->success(__('Imagem editada com sucesso'));
                        return $this->redirect(['controller' => 'EmpresasSobre', 'action' => 'view', $id]);
                    }else{
                        $empresasSobre->imagem = $imagemAntiga;
                        $this->Users->save($empresasSobre);
                        $this->Flash->danger(__('Erro: Imagem não foi editada com sucesso. Erro ao realizar o upload'));
                    }
                }else{
                    $this->Flash->danger(__('Erro: Imagem não foi editada com sucesso.'));
                }
            }else{
                $this->Flash->danger(__('Erro: Imagem não foi editada com sucesso.'));
            }          
            
        }  

        $this->set(compact('empresasSobre'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Empresas Sobre id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $empresasSobre = $this->EmpresasSobre->get($id);
        $destino = WWW_ROOT."files".DS."sobre_empresas".DS.$empresasSobre->id.DS;
        $this->EmpresasSobre->deleteArq($destino);
        $empresasSobreTable = TableRegistry::get('EmpresasSobre');
        $sobEmpProx = $empresasSobreTable->getListaImgSobEmpProx($empresasSobre->ordem);
        if ($this->EmpresasSobre->delete($empresasSobre)) {
            foreach($sobEmpProx as $imgProx){
                $empresasSobre->ordem = $imgProx->ordem - 1;
                $empresasSobre->id = $imgProx->id;
                $this->EmpresasSobre->save($empresasSobre);
            }
            $this->Flash->success(__('Sobre Empresas apagado com sucesso'));
        } else {
            $this->Flash->error(__('Erro: Sobre Empresas não foi apagado com sucesso'));
        }

        return $this->redirect(['controller' => 'EmpresasSobre', 'action' => 'index']);
    }
}
