<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
/**
 * Home Controller
 *
 * @property \App\Model\Table\UsersTable $Home
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HomeController extends AppController
{
    // Atribuindo permissão para acessar a página index.ctp para acessar o site 
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['index']);
    }

    public function index()
    {
        $carouselTable = TableRegistry::get('Carousels');
        $carousels = $carouselTable->getListarSlideHome();

        $servicosTable = TableRegistry::get('Servicos');
        $servicos = $servicosTable->getListarServicoSlideHome('1');
        
        $depoimentosTable = TableRegistry::get('Depoimentos');
        $depoimentos = $depoimentosTable->getIdDepoimentos();
        $depoimentos = $depoimentosTable->getListarDepHome($depoimentos->id);

        $this->set(compact('carousels', 'servicos', 'depoimentos'));
    }
}
