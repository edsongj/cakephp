<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
/**
 * Contato Controller
 *
 * @property \App\Model\Table\UsersTable $Contato
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContatoController extends AppController
{
    // Atribuindo permissão para acessar a página index.ctp para acessar o site 
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['index']);
    }

    public function index()
    {
        $contato = "Contato";
        $this->set(compact('Contato'));
    }
}
