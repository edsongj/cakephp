<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
/**
 * SobreEmpresa Controller
 *
 * @property \App\Model\Table\UsersTable $SobreEmpresa
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SobreEmpresaController extends AppController
{
    // Atribuindo permissão para acessar a página index.ctp para acessar o site 
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['index']);
    }

    public function index()
    {
        $sobreEmpresa = "Sobre Empresa";
        $this->set(compact('sobreEmpresa'));
    }
}
