<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * EmpresasSobre Model
 *
 * @property \App\Model\Table\SituationsTable&\Cake\ORM\Association\BelongsTo $Situations
 *
 * @method \App\Model\Entity\EmpresasSobre get($primaryKey, $options = [])
 * @method \App\Model\Entity\EmpresasSobre newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\EmpresasSobre[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\EmpresasSobre|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EmpresasSobre saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EmpresasSobre patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\EmpresasSobre[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\EmpresasSobre findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class EmpresasSobreTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('empresas_sobre');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Upload');
        $this->addBehavior('UploadRed');
        $this->addBehavior('DeleteArq');

        $this->belongsTo('Situations', [
            'foreignKey' => 'situation_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('titulo')
            ->maxLength('titulo', 220)
            ->notEmptyString('titulo');

        $validator
            ->scalar('descricao')
            ->notEmptyString('descricao');

        $validator
            ->notEmpty('imagem', 'Necessário selecionar a foto')
            ->add('imagem', 'file', [
                'rule' => ['mimeType', ['image/jpeg', 'image/png']],
                'message' => 'Extensão da foto inválida. Selecione foto JPEG ou PNG',
            ]);

        $validator
            ->integer('ordem')
            ->notEmpty('ordem');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['situation_id'], 'Situations'));

        return $rules;
    }
    public function getUltimoImgEmp()
    {
        $query = $this->find()
                    ->select(['id', 'ordem'])
                    ->order(['EmpresasSobre.ordem' => 'DESC']);
        return $query->first();
    }
    public function getListaImgSobEmpProx($ordem)
    {
        $query = $this->find()
                    ->select(['id', 'ordem'])
                    ->where(['EmpresasSobre.ordem >' => $ordem])
                    ->order(['EmpresasSobre.ordem' => 'ASC']);
        return $query;
    }
    public function getImgAtual($id)
    {
        $query = $this->find()
                    ->select(['id', 'ordem'])
                    ->where(['EmpresasSobre.id =' => $id])
                    ->order(['EmpresasSobre.ordem' => 'DESC']);
        return $query->first();
    }
    public function getImgMenor($ordemMenor)
    {
        $query = $this->find()
                    ->select(['id', 'ordem'])
                    ->where(['EmpresasSobre.ordem =' => $ordemMenor])
                    ->order(['EmpresasSobre.ordem' => 'DESC']);
        return $query->first();
    }
}
