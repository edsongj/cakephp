<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Servicos Model
 *
 * @method \App\Model\Entity\Servico get($primaryKey, $options = [])
 * @method \App\Model\Entity\Servico newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Servico[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Servico|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Servico saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Servico patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Servico[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Servico findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ServicosTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('servicos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('titulo_ser')
            ->maxLength('titulo_ser', 220)
            ->notEmptyString('titulo_ser', 'Obrigatório informar o Título');

        $validator
            ->scalar('icone_um')
            ->maxLength('icone_um', 45)
            ->notEmptyString('icone_um', 'Obrigatório informar o Icone Um');

        $validator
            ->scalar('titulo_um')
            ->maxLength('titulo_um', 220)
            ->notEmptyString('titulo_um', 'Obrigatório informar o Título Um');

        $validator
            ->scalar('descricao_um')
            ->notEmptyString('descricao_um', 'Obrigatório informar a Descrição Um');

        $validator
            ->scalar('icone_dois')
            ->maxLength('icone_dois', 45)
            ->notEmptyString('icone_dois', 'Obrigatório informar o Icone Dois');

        $validator
            ->scalar('titulo_dois')
            ->maxLength('titulo_dois', 220)
            ->notEmptyString('titulo_dois', 'Obrigatório informar o Título Dois');

        $validator
            ->scalar('descricao_dois')
            ->notEmptyString('descricao_dois', 'Obrigatório informar a Descrição Dois');

        $validator
            ->scalar('icone_tres')
            ->maxLength('icone_tres', 45)
            ->notEmptyString('icone_tres', 'Obrigatório informar o Icone Três');

        $validator
            ->scalar('titulo_tres')
            ->maxLength('titulo_tres', 220)
            ->notEmptyString('titulo_tres', 'Obrigatório informar o Título Três');

        $validator
            ->scalar('descricao_tres')
            ->notEmptyString('descricao_tres', 'Obrigatório informar a Descrição Três');

        return $validator;
    }

    public function getListarServicoSlideHome($id)
    {
        $query = $this->find()
                    ->select(['id', 'titulo_ser', 'icone_um', 'titulo_um', 'descricao_um', 'icone_dois', 'titulo_dois', 'descricao_dois', 'icone_tres', 'titulo_tres', 'descricao_tres', ])
                    ->where(['Servicos.id =' => $id]);
        return $query->first();
    }
}
