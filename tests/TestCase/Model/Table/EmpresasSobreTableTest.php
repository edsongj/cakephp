<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EmpresasSobreTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EmpresasSobreTable Test Case
 */
class EmpresasSobreTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\EmpresasSobreTable
     */
    public $EmpresasSobre;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.EmpresasSobre',
        'app.Situations'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('EmpresasSobre') ? [] : ['className' => EmpresasSobreTable::class];
        $this->EmpresasSobre = TableRegistry::getTableLocator()->get('EmpresasSobre', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->EmpresasSobre);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
